//
//  ViewController.swift
//  TaskWeather
//
//  Created by sushil kumar on 18/05/18.
//  Copyright © 2018 effone.comeffone. All rights reserved.
//

import UIKit
import CoreLocation


class ViewController: UIViewController ,CLLocationManagerDelegate{
    
    //MARK: - IBOutlets
    @IBOutlet weak var cityNameLbl: UILabel!
    @IBOutlet weak var tempratureLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var typeWeather: UILabel!
    @IBOutlet weak var bgImageView: UIImageView!
    
    //MARK: - Variables
    lazy var currentWeather = CurrentWeather()
    var currentLocation: CLLocation!
    //MARK: - Constant
    let locationManager = CLLocationManager()
    
    //MARK: - ViewCycle Function
    override func viewDidLoad() {
        super.viewDidLoad()
        bgImageView.addBlurEffect()
        locationManager.delegate = self
        locationSetting()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        authorizationSetting()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Functions
    func updateUI() {
        if let name = currentWeather.cityName
        {
        cityNameLbl.text = name
        }
        if let date = currentWeather.myDate
        {
        dateLbl.text = date
        }
        tempratureLbl.text = "\(Int(currentWeather.temp)) °C"
        if let weather = currentWeather.typeWeather
        {
            typeWeather.text = weather
        }
        
    }
    func locationSetting() {
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startMonitoringSignificantLocationChanges()
    }
    func authorizationSetting() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse
        {
            currentLocation = locationManager.location
            print(currentLocation.coordinate.latitude, currentLocation.coordinate.longitude)
            Location.sharedInstance.longitute = currentLocation.coordinate.longitude
            Location.sharedInstance.latitude = currentLocation.coordinate.latitude
            currentWeather.getCurrentTemperature {
                self.updateUI()
            }
        }
        else
        {
            locationManager.requestWhenInUseAuthorization()
            authorizationSetting()
            
        }
    }
}


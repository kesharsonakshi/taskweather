//
//  CurrentWeather.swift
//  TaskWeather
//
//  Created by sushil kumar on 18/05/18.
//  Copyright © 2018 effone.comeffone. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class CurrentWeather
{
    var cityName : String?
    var typeWeather : String?
    var temp :  Double = 0.0
    var myDate : String?
    
    func getCurrentTemperature(completion : @escaping completion)
    {
        Alamofire.request(WEATHER_URL).responseJSON { (response) in
            let result = response.result
            let json = JSON(result.value)
            self.cityName = json["name"].stringValue
            self.typeWeather = json["weather"][0]["main"].stringValue
            let tempdate = json["dt"].doubleValue
            let convertedDate = Date(timeIntervalSince1970: tempdate)
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .medium
            dateFormatter.timeStyle = .none
            self.myDate = dateFormatter.string(from: convertedDate)
            let temperature = json["main"]["temp"].doubleValue
            self.temp = (temperature - 273.15).rounded(toPlaces: 0)
            completion()
        }
        
    }
}

